import Koa from 'koa';
import { User } from '../models/user';
import { userMiddleware, validateId } from '../middleware/user_middleware';
import { BodyUser } from '../interfaces/user_interface';

export class UserController {

    users: Array<User> = [];

    getUsers( ctx: Koa.Context ) {
        
        ctx.status = 200;
        
        if ( this.users.length === 0 ) {

            ctx.body = {
                ok: false,
                message: 'No users registered'
            };    

            return; 
        }

        ctx.body = this.users
    }

    createUser( ctx: Koa.Context ) {
        
        const body: BodyUser  = ctx.request.body;           
        const error = userMiddleware( body );

        if ( error ) {

            ctx.status = error.status;
            ctx.body = error;

            return;
        }

        let userCreated = new User( body.name, body.surname, body.age, this.users.length + 1 );

        this.users.push( userCreated );

        ctx.status = 201;
        ctx.body = userCreated; 
    }

    updateUser( ctx: Koa.Context ) {

        const { id } = ctx.params;
        const body: BodyUser  = ctx.request.body;

        const errorUser = userMiddleware( body );
        const error = validateId( id );
        
        if ( error ) {

            ctx.status = error.status;
            ctx.body = error;

            return;
        }

        if ( errorUser ) {

            ctx.status = errorUser.status;
            ctx.body = errorUser;

            return;
        }

        this.users = this.users.map(( user ) => {

            if ( user.idUser === Number.parseInt( id ) ) {
                return {
                    ...user,
                    name: body.name,
                    surname: body.surname,
                    age: body.age
                };
            }

            return user;
        });

        ctx.status = 200;
        ctx.body = {
            message: 'the userId ' + id + ' has been updated succesfully',
            ok: true
        };
    }

    deleteUser( ctx: Koa.Context ) {

        const { id } = ctx.params;

        const error = validateId( id );
        
        if ( error ) {

            ctx.status = error.status;
            ctx.body = error;

            return;
        }

        this.users = this.users.filter( ( user ) => user.idUser !== Number.parseInt( id ) );

        ctx.status = 200;
        ctx.body = {
            message: 'the userId ' + id + ' has been removed succesfully',
            ok: true
        };
    }
}