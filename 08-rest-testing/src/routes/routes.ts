import Router from 'koa-router';

import { UserController } from '../controllers/index';

const router = new Router();
const userController = new UserController();

router.get('/users',  userController.getUsers.bind( userController ) );
router.post('/users', userController.createUser.bind( userController ) );
router.delete('/users/:id', userController.deleteUser.bind( userController ) );
router.put('/users/:id', userController.updateUser.bind( userController ) );

export { router };