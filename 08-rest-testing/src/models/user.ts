export class User {

    name: string;
    surname: string;
    age: number;
    idUser: number | null;

    constructor( name: string, surname: string, age: number, idUser?: number ) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.idUser = idUser || null;
    }
}