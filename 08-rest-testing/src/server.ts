import Koa from 'koa';
import bodyParser from 'koa-bodyparser';
import json from 'koa-json';
import { router } from './routes/routes';

export class Server {

    app: Koa;
    port: number;

    constructor( port: number ) {

        this.app = new Koa();
        this.port = port;

        this.setMiddlewares();
    }
    
    setMiddlewares() {

        this.app
            .use( bodyParser() )
            .use( json() )
            .use( router.routes() )
            .use( router.allowedMethods() )
    }

    run() {

        this.app.listen( 
            this.port, 
            () => console.log( 'Servidor corriendo en el puerto ' + this.port.toString() ) 
        );
    }
}