export interface BodyUser {
    idUser?: number,
    name: string,
    surname: string,
    age: number
};