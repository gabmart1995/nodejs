"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.router = void 0;
const koa_router_1 = __importDefault(require("koa-router"));
const index_1 = require("../controllers/index");
const router = new koa_router_1.default();
exports.router = router;
const userController = new index_1.UserController();
router.get('/users', userController.getUsers.bind(userController));
router.post('/users', userController.createUser.bind(userController));
router.delete('/users/:id', userController.deleteUser.bind(userController));
router.put('/users/:id', userController.updateUser.bind(userController));
