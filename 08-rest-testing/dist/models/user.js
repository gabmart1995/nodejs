"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.User = void 0;
class User {
    constructor(name, surname, age, idUser) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.idUser = idUser || null;
    }
}
exports.User = User;
