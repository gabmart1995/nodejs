"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserController = void 0;
const user_1 = require("../models/user");
const user_middleware_1 = require("../middleware/user_middleware");
class UserController {
    constructor() {
        this.users = [];
    }
    getUsers(ctx) {
        ctx.status = 200;
        if (this.users.length === 0) {
            ctx.body = {
                ok: false,
                message: 'No users registered'
            };
            return;
        }
        ctx.body = this.users;
    }
    createUser(ctx) {
        const body = ctx.request.body;
        const error = user_middleware_1.userMiddleware(body);
        if (error) {
            ctx.status = error.status;
            ctx.body = error;
            return;
        }
        let userCreated = new user_1.User(body.name, body.surname, body.age, this.users.length + 1);
        this.users.push(userCreated);
        ctx.status = 201;
        ctx.body = userCreated;
    }
    updateUser(ctx) {
        const { id } = ctx.params;
        const body = ctx.request.body;
        const errorUser = user_middleware_1.userMiddleware(body);
        const error = user_middleware_1.validateId(id);
        if (error) {
            ctx.status = error.status;
            ctx.body = error;
            return;
        }
        if (errorUser) {
            ctx.status = errorUser.status;
            ctx.body = errorUser;
            return;
        }
        this.users = this.users.map((user) => {
            if (user.idUser === Number.parseInt(id)) {
                return Object.assign(Object.assign({}, user), { name: body.name, surname: body.surname, age: body.age });
            }
            return user;
        });
        ctx.status = 200;
        ctx.body = {
            message: 'the userId ' + id + ' has been updated succesfully',
            ok: true
        };
    }
    deleteUser(ctx) {
        const { id } = ctx.params;
        const error = user_middleware_1.validateId(id);
        if (error) {
            ctx.status = error.status;
            ctx.body = error;
            return;
        }
        this.users = this.users.filter((user) => user.idUser !== Number.parseInt(id));
        ctx.status = 200;
        ctx.body = {
            message: 'the userId ' + id + ' has been removed succesfully',
            ok: true
        };
    }
}
exports.UserController = UserController;
