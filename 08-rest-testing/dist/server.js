"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Server = void 0;
const koa_1 = __importDefault(require("koa"));
const koa_bodyparser_1 = __importDefault(require("koa-bodyparser"));
const koa_json_1 = __importDefault(require("koa-json"));
const routes_1 = require("./routes/routes");
class Server {
    constructor(port) {
        this.app = new koa_1.default();
        this.port = port;
        this.setMiddlewares();
    }
    setMiddlewares() {
        this.app
            .use(koa_bodyparser_1.default())
            .use(koa_json_1.default())
            .use(routes_1.router.routes())
            .use(routes_1.router.allowedMethods());
    }
    run() {
        this.app.listen(this.port, () => console.log('Servidor corriendo en el puerto ' + this.port.toString()));
    }
}
exports.Server = Server;
