"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.validateId = exports.userMiddleware = void 0;
const regex = {
    onlyLetters: /^[A-Za-z]+$/,
    onlyNumbers: /^[0-9]+$/
};
function validateId(idUser) {
    if (!regex.onlyNumbers.test(idUser)) {
        return {
            error: 'the user id is not valid',
            status: 400,
            ok: false
        };
    }
}
exports.validateId = validateId;
function userMiddleware(body) {
    if (!regex.onlyLetters.test(body.name)) {
        return {
            error: 'the name is not valid',
            status: 400,
            ok: false
        };
    }
    if (!regex.onlyLetters.test(body.surname)) {
        return {
            ok: false,
            error: 'the surname is not valid',
            status: 400
        };
    }
    if (!regex.onlyNumbers.test(body.age.toString())) {
        return {
            ok: false,
            error: 'the age is not valid',
            status: 400
        };
    }
    else if (body.age < 17 || body.age > 90) {
        if (body.age < 17) {
            return {
                ok: false,
                error: 'the age is too young',
                status: 400
            };
        }
        else {
            return {
                ok: false,
                error: 'the age is too old',
                status: 400
            };
        }
    }
}
exports.userMiddleware = userMiddleware;
