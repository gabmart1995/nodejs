const opcionesCrear = {

	descripcion: {
		alias: 'd',
		demand: true,
		desc: 'Descripcion de la tarea'
	}
};

const opcionesActualizar = {

	descripcion: opcionesCrear.descripcion,

	completado: {
		alias: 'c',
		default: true,
		demand: true,
		desc: 'Marca como completado o pendiente la tarea'
	}

};

const opcionesListar = {

	completado: {
		alias: opcionesActualizar.completado.alias,
		desc: 'Realiza una búsqueda de las tareas pendientes o completadas'
	}
}

const argv = require( 'yargs' )
	.command( 'crear', 'Crea un elemento por hacer', opcionesCrear )
	.command( 'actualizar', 'Actualiza el estado de una tarea', opcionesActualizar )
	.command( 'listar', 'Muestra la toda la lista de las tareas por hacer', opcionesListar )
	.command( 'borrar', 'Elimina una tarea existente', opcionesCrear )
	.help()
	.argv;

// exporta el modulo
module.exports = {
	argv
}