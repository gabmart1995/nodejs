const fs = require( 'fs' );

let listadoPorHacer = []; // arreglo de tareas

const crear = ( descripcion ) => {

	cargarDB();

	let porHacer = {
		descripcion,
		completado: 'false'
	};

	listadoPorHacer.push( porHacer );

	guardarDB(); // guarda en el archivo JSON

	return porHacer;

}

const obtenerListado = () => {

	cargarDB();

	return listadoPorHacer;
}

const obtenerListadoEspecifico = ( completado ) => {

	cargarDB();

	// crea un nuevo arreglo de consulta filtrando el resultado
	let nuevoListado =  listadoPorHacer.filter( tarea => tarea.completado === completado );

	return nuevoListado;
}

const guardarDB = () => {

	let data = JSON.stringify( listadoPorHacer );

	fs.writeFile( './db/data.json', data, ( error ) => {

		if ( error ){
			throw new Error( 'No se pudo grabar el archivo', error )	
		}
	});
}

const actualizar = ( descripcion, completado = true ) => {

	cargarDB();

	// busca la posicion de la tarea
	let index = listadoPorHacer.findIndex( tarea => tarea.descripcion ===  descripcion );

	if ( index >= 0 ) {

		listadoPorHacer[ index ].completado = completado;
		guardarDB();

		return true;
	}

	else {
		return false;
	}
}

const borrar = ( descripcion ) => {

	cargarDB();

	let nuevoListado = listadoPorHacer.filter( tarea => tarea.descripcion !== descripcion );

	if ( listadoPorHacer.length === nuevoListado.length ) {

		return false;
	}

	else {
		
		listadoPorHacer = nuevoListado;
		guardarDB();

		return true;
	}

}

const cargarDB = () => {

	try {

		listadoPorHacer = require( './../db/data.json' );
	} 
	catch ( error ) {

		listadoPorHacer = []; // si el JSON esta vacio
	}

}

module.exports = {
	crear,
	obtenerListado,
	obtenerListadoEspecifico,
	actualizar,
	borrar
}