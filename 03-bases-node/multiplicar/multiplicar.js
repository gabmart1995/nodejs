const fs = require( 'fs' );  // librería nativa de node para crear archivos
const colors = require( 'colors' );	

let crearArchivo = ( base, limite = 10 ) => {

	return new Promise( ( resolve, reject ) => {

		if ( !Number( base ) ) {
			
			reject( `El valor introducido ${ base } o ${ limite } no es un número` );
			return;
		}

		else if ( !Number( limite) ) {
			
			reject( `El valor introducido como limite ${ limite } no es un número.` );
			return;
		}

		let data = '';

		for ( let i = 1; i <= limite; i++ ) {
			data += `${ base } * ${ i } = ${ base * i }\n`;
		} 

		// se construye el archivo con la tabla de base
		fs.writeFile( `./tablas/tabla-${ base }-al-${ limite }.txt`, data, ( error ) => {

			if ( error ){
				reject( error );	
			} 

			else {
				resolve( `tabla-${ base }-al-${ limite }.txt` );
			}
		});

	});
}

let listarTabla = ( base, limite = 10 ) => {

	return new Promise( ( resolve, reject ) => {

		if ( !Number( base ) ) {
			
			reject( `El valor introducido como base ${ base } no es un número` );
			return;
		}

		 else if ( !Number( limite) ) {
			
			reject( `El valor introducido como limite ${ limite } no es un número.` );
			return;
		}

		console.log('===================='.green );
		console.log(`tabla de ${ base } `.green );
		console.log('===================='.green );

		for ( let i = 1; i <= limite; i++ ) {
			console.log( `${ base } * ${ i } = ${ base * i }` );
		}
	}); 
}

// permite exportar el archivo
module.exports = {
	crearArchivo,
	listarTabla
};
