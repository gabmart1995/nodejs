/*
	Variables globales de node:

	module: permite exportar clases y funciones para ser usado por archivos externos
	process: permite obtener informacion sobre los procesos del sistema operativo
	
	Yargs: permite crear rapidamente aplicaciones en consola muy útil para el 
	desarrollo agil asignado parametros por consola ver linea 2

	// let parametro = argv[2];
	// let base = parametro.split( '=' )[1];  // separa la cadena
	// let argv2 = process.argv;  // obtiene las variables de entorno de nodeJS

	//console.log( 'base: ',  argv.base );
	//console.log( 'limite: ', argv.limite );

*/

const argv = require( './config/yargs' ).argv;
const colors = require( 'colors' );

const { crearArchivo, listarTabla } = require( './multiplicar/multiplicar' );

let comando = argv._[0];

switch ( comando ) {

	case 'listar':
		
		listarTabla( argv.base, argv.limite )
			.catch( error => console.log( error.red ) );	

	break;

	case 'crear':
		
		crearArchivo( argv.base, argv.limite )
			.then( archivo => console.log( 'Archivo creado:', colors.green( archivo ) ) )
			.catch( error => console.log( error.red ) );

	break;

	default:
		console.log( 'Comando no reconocido'.red );
	break;

}