const argv = require( './config/yargs' ).argv;

const lugar = require( './lugar/lugar' );
const clima = require( './clima/clima' );

const getInfo = async ( direccion ) => {

	try {

		let coordenadas = await lugar.getLugarLatLong( direccion );
		let temperatura = await clima.getClima( coordenadas.lat, coordenadas.lon );

		return `El clima de ${ coordenadas.direccion } es de ${ temperatura.clima }°C`;

	} catch ( e ) {

		return `No se pudo determinar el clima de ${ direccion }`;
	}
}

getInfo( argv.direccion )
	.then( response => console.log( response ) )
	.catch( error => console.log( error ) );
