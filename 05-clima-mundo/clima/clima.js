const axios = require( 'axios' );

const getClima = async ( lat, lon ) => {

	let response = await axios.get(`https://api.openweathermap.org/data/2.5/weather?lat=${ lat }&lon=${ lon }&appid=43d7b14f3b197932d2b31ebc383436d4&units=metric`);

	return { clima: response.data.main.temp };
}

module.exports = {
	getClima
}