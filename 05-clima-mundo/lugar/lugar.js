const axios = require( 'axios' );

const getInstance = ( encodedURL ) => {

	// parametros de conexion al API.
	let instance = axios.create({

		baseURL: `https://devru-latitude-longitude-find-v1.p.rapidapi.com/latlon.php?location=${ encodedURL }`,
		headers: {
			'x-rapidapi-key': '9bbf3d6e47msh448a03ce70d9b89p115b47jsnff9f832235c6'
		}
	});

	return instance;
}

const getLugarLatLong = async ( dir ) => {

	let encodedURL = encodeURI( dir ); // URL segura

	let instance = getInstance( encodedURL );

	// ejecuta la peticion
	let response = await  instance.get();

	if ( response.data.Results.length === 0 ) {

		throw new Error(`No hay resultados para esa ${ dir }`);
	}

	let data = response.data.Results[0]; 

	let direccion = data.name;
	let lat = data.lat;
	let lon = data.lon;

	return {
		direccion,
		lat,
		lon
	}

}


module.exports = {
	getLugarLatLong
}