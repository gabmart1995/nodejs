# CURSO DE NODEJS DE CERO A EXPERTO:
	
## Creditos:
	
Curso realizado por Fernando Herrera en Udemy 02-08-19 contenido:

- Fundamentos de node
- Repaso de EcmaScript 6 y 7
- Bases de node
- Aplicacion de tareas
- Aplicacion del clima 
- Webserver
- Rest server
- Alcances del rest server
- JWT
- Google sign in
- Categorias y productos
- Cargas de archivos
- Sockets
- Node + MySql + TypeScript

Lo necesario de cero a experto para la construccion de aplicaciones con JavaScript.