let nombre = 'Deadpool';
let real = 'Wade Winston';

let nombreTemplate = `${ nombre } ${ real }`;
let nombreCompleto = nombre + ' ' + real; 

// console.log( nombreCompleto === nombreTemplate );

function getNombre() {
	
	return `${ nombre } es ${ real }`;
}

console.log( `El nombre de: ${ getNombre() }` );