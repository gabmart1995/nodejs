let deadpool = {
	
	nombre: 'Wade',
	apellido: 'Winston',
	poder: 'Regeneracion',

	getNombre: function() {
		return `${ this.nombre } ${ this.apellido } - poder: ${ this.poder }`;
	} 
};

let person = {
	
	nombre: 'Gabriel',
	apellido: 'Martinez',
	poder: 'Ninguno',

	getNombre: function() {
		return `${ this.nombre } ${ this.apellido } - poder:  ${ this.poder }`;
	}
}

// destructuracion de objetos + asignacion de una nueva variable

let { nombre, apellido, poder } = deadpool;
let { nombre: segundoNombre, apellido: segundoApellido, poder: segundoPoder } =  person;

console.log( nombre, apellido, poder );
console.log( segundoNombre, segundoApellido, segundoPoder );