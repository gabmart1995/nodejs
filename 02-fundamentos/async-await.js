/*
	Async - Await

	Async: permite la creacion de funciones asincronas dentro de la 
	aplicacion que se ejecutan en el mismo hilo.

	Await: es una condicion especial que permite deterner la ejecicion de la
	funcion mientras espera la respuesta de la ejecución de la metodo que
	se invoca.   
*/

let getNombre = async() => {
	
	return 'Gabriel';
	// throw new Error( 'No existe un nombre para ese usuario' );  error
};

// es lo mismo que:
let getNombre2 = () => {
	
	return new Promise( ( resolve, reject ) => {
		
		setTimeout( () => {
			resolve( 'Gabriel' );
		}, 3000);
	});
}


let saludo = async () => {
	
	let nombre = await getNombre2();  // espera 3 segundos y obtiene la respuesta

	return `Hola ${ nombre }`;
}


saludo()
	.then( mensaje => console.log( mensaje ) )
