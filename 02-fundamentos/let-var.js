let nombre = 'Wolverine';

if ( true ) {
	nombre = 'Magneto'; 
}

// var: puede ser reinicializada cierta cantidad de veces
// let: solo puede inicializada una vez y se utiliza en ambito especial

console.log( nombre );

for (let i = 0; i <= 5; i++) {
	
	console.log( `i: ${ i }` );
}