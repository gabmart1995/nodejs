/*
* 	Promesas:
*
*	Las promesas nos permiten ejecutar un trabajo asincrono y despues que se
*	resuleve la tarea establecer una respuesta en particular
*/


let empleados = [
	{
		id: 1,
		nombre: 'Gabriel'
	},
	{
		id: 2,
		nombre: 'Melissa'
	},
	{
		id: 3,
		nombre: 'Fernando'
	}
]

// salarios
let salarios = [
	{
		id: 1,
		salario: 1000
	},
	{
		id: 2,
		salario: 2000
	},
]

let getEmpleado = ( id ) => {

	return new Promise( ( resolve, reject ) => {

		let empleadoDB = empleados.find( empleado => empleado.id === id ); 

		if ( !empleadoDB ) {
			reject( `No existe el usuario con el id: ${ id }` );
		} 

		else {
			resolve( empleadoDB );
		}
	});
}

let getSalario = ( empleado ) => {

	return new Promise( ( resolve, reject ) => {
		
		let salarioDB = salarios.find( salario => empleado.id === salario.id );

		if ( !salarioDB ) {
			reject( `No se encontró un salario para el empleado ${ empleado.nombre }` );
		}

		else {
			resolve({
				nombre: empleado.nombre,
				salario: salarioDB.salario
			});
		}
	});
} 

// ejecucion de promesas en cadena
getEmpleado( 1 )
	.then( empleado => {
		return getSalario( empleado );	// promesa 1
	})
	.then( response => {
		console.log( `El salario de ${ response.nombre } es de ${ response.salario }` );
	})
	.catch( ( error ) => {
		console.log( error );
	});
	