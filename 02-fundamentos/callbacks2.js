// empleados
let empleados = [
	{
		id: 1,
		nombre: 'Gabriel'
	},
	{
		id: 2,
		nombre: 'Melissa'
	},
	{
		id: 3,
		nombre: 'Fernando'
	}
]

// salarios
let salarios = [
	{
		id: 1,
		salario: 1000
	},
	{
		id: 2,
		salario: 2000
	},
]

let getEmpleado = ( id, callback ) => {

	let empleadoDB = empleados.find( empleado => empleado.id === id ); 

	if ( !empleadoDB ) {
		callback( `No existe el usuario con el id: ${ id }` );
	} 

	else {
		callback( null, empleadoDB );
	}
}

let getSalario = ( empleado, callback ) => {

	let salarioDB = salarios.find( salario => empleado.id === salario.id );

	if ( !salarioDB ) {
		callback( `No se encontró un salario para el empleado ${ empleado.nombre }` );
	}

	else {

		callback( null, {
			nombre: empleado.nombre,
			salario: salarioDB.salario,
		});
	}
} 


// llama a la funcion
getEmpleado( 3, ( error, empleado ) => {
	
	if ( error ) {
		return console.log( error );
	}

	getSalario( empleado, ( error, response ) => {

		if ( error ) {
			return console.log( error );
		}

		console.log( `El empleado ${ response.nombre }, es de ${ response.salario }$` );
	});
});