getUsuarioById = ( id, callback ) => {

	let usuario = {
		nombre: 'Gabriel',
		id
	};

	if ( id === 20 ) {  // error
		callback( `El usuario con el id ${ id }, no existe dentro de la BD` );
	}

	else {
		// genera la respuesta del usuario
		callback( null, usuario );
	}
};

getUsuarioById( 1, ( error, usuario ) => {

	if ( error ) {
		return console.log( error );
	}

	console.log('Usuario de la base de datos: ', usuario );
}); 

