// funcion de flecha
let sumar = ( a, b ) => a + b;
let saludo = () => 'Hola Mundo';
let saludo2 = ( nombre ) => `Hola ${ nombre }`;

console.log( sumar( 10, 20 ) );
console.log( saludo() );
console.log( saludo2( 'Gabriel' ) );

let deadpool = {
	
	nombre: 'Wade',
	apellido: 'Winston',
	poder: 'Regeneracion',

	getNombre() {
		return `${ this.nombre } ${ this.apellido } - poder: ${ this.poder }`;
	} 
};

// una caracteristica principal de las funciones de flecha es que apunta al valor
// que tenga el objeto this fuera de la funcion

console.log( deadpool.getNombre() );