const express = require( 'express' );
const app = express();

const hbs = require( 'hbs' );

require( './hbs/helpers' );

// variables de entorno de Heroku
const port = process.env.PORT || 3000;

// creacion del middleware para declarar el directorio 
//publico del servidor

app.use( express.static( __dirname + '/public' ) );

// Express HBS engine (Motor de plantillas de Express)
// se declara el directorio de los templates

hbs.registerPartials( __dirname + '/views/partials' );  
app.set( 'view engine', 'hbs' );

// rutas de la aplcacion
app.get( '/', ( request, response ) => {

	response.render( 'home', {
		nombre: 'gabriel',
	});
});

app.get('/json', ( request, response ) => {

	let salida = {
		nombre: 'Gabriel',
		edad: 24,
		url: request.url
	};

	// imprime el JSON
	response.send( salida );
});

app.get( '/about', ( request, response ) => {

	response.render( 'about' );
});

app.listen( port, () => {
	console.log( `Escuchando peticiones en el puerto ${ port }` );
});