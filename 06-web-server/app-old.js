const http = require( 'http' );

// crea el servidor
http.createServer( ( request, response ) => {

	
	response.writeHead( 200, { 'content-type': 'application/json' } );

	let salida = {
		nombre: 'Gabriel',
		edad: 24,
		url: request.url
	}

	response.write( JSON.stringify( salida ) );
	response.end();

}).listen( 8080 );

console.log( 'Escuchando en el puerto 8080' );
