const jwt = require( 'jsonwebtoken' );

// ===========================================
//	Verificar token
// ===========================================
let verificaToken = ( request, response, next ) => {

	let token = request.get( 'token' );

	jwt.verify( token, process.env.SEED, ( error, decoded ) => {

		if ( error ) {

			return response.status( 401 ).json({
				ok: false,
				error
			});
		}

		request.usuario = decoded.usuario;
		next();
	});
};

module.exports = {
	verificaToken
};