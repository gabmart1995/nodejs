const express = require( 'express' );
const bcrypt = require( 'bcrypt' );
const jwt = require( 'jsonwebtoken' );

const Usuario = require( './../models/usuario' );
const app = express();

app.post( '/login', ( request, response ) => {

	let body = request.body;

	Usuario.findOne({ email: body.email }, ( error, usuarioBD ) => {

		if ( error ) {
			
			return response.status( 500 ).json({
				ok: false,
				err: {
					message: 'Token no valido'
				}
			});
		}

		if ( !usuarioBD ) {
			
			return response.status( 400 ).json({
				ok: false,
				err: {
					messaje: 'Usuario o contraseña incorrecta'
				}
			});
		}

		// validacion de contraseña
		if ( !bcrypt.compareSync( body.password, usuarioBD.password ) ) {
			
			return response.status( 400 ).json({
				ok: false,
				err: {
					messaje: 'Usuario o contraseña incorrecta'
				}
			});			
		}

		// se genera el token
		let token  = jwt.sign( 
			{ usuario: usuarioBD }, 
			process.env.SEED, 
			{ expiresIn: process.env.CADUCIDAD_TOKEN } 
		);

		//OK
		response.json({
			ok: true,
			usuario: usuarioBD,
			token
		});

	});
});


module.exports = app;