const express = require( 'express' );
const app = express();

// encriptacion de datos
const bcrypt = require( 'bcrypt' );
const _ = require( 'underscore' );

// modelo de usuario
const Usuario = require( './../models/usuario' );

//middlewares del token
const { verificaToken } = require( './../middleware/autenticacion' );

// ===========================================
//	Obtiene todos los usuarios activos
// ===========================================
app.get( '/usuario', verificaToken, ( request, response ) => {
	
	let desde = Number( request.query.desde || 0 );
	let limite = Number( request.query.limite || 5 );

	Usuario.find( { estado: true }, 'nombre email role estado google img' )
		.skip( desde )
		.limit( limite )
		.exec( ( error, usuarios ) => {

			if ( error ) {

				return response.status( 400 ).json({
					ok: false,
					err: error
				});
			}

			Usuario.count( { estado: true }, ( error, count ) => {

				response.json({
					ok: true,
					usuarios,
					numeroDeUsuarios: count
				});
			});
		});
}); 


// ===========================================
//	Crea un nuevo usuario
// ===========================================
app.post( '/usuario', verificaToken, ( request, response ) => {
	
	let body = request.body;

	let usuario = new Usuario({

		nombre: body.nombre,
		email: body.email,
		password: bcrypt.hashSync( body.password, 10 ),
		role: body.role
	});

	// guarda el registro
	usuario.save( ( error, usuarioDB ) => {

		if ( error ) {

			return response.status( 400 ).json({
				ok: false,
				err: error
			});
		}

		response.json({
			ok: true,
			usuario: usuarioDB
		});
	});
}); 

// ===========================================
//	Actualiza un usuario
// ===========================================
app.put( '/usuario/:id', verificaToken, ( request, response ) => {

	let id = request.params.id;
	let camposActualizar = [ 'nombre', 'email', 'img', 'role', 'estado' ];
	let body = _.pick( request.body, camposActualizar );
	
	let updateOptions = {
		new: true,
		runValidators: true
	}
	
	Usuario.findByIdAndUpdate( id, body, updateOptions, ( error, usuarioDB ) => {

		if ( error ) {

			return response.status( 400 ).json({
				ok: false,
				err: error
			});
		}

		if ( !usuarioDB ) {

			return response.status( 400 ).json({
				ok: false,
				error: {
					message: 'Usuario no encontrado'
				}
			});	
		}

		response.json({ 
			ok: true,
			usuario: usuarioDB
		});
	});
}); 

// ===========================================
//	Elimina un usuario
// ===========================================
app.delete( '/usuario/:id', verificaToken, ( request, response ) => {
	
	let id = request.params.id;
	let cambiarEstado = { estado: false };
	let updateOptions = { new: true };

	// Usuario.findByIdAndRemove( id, ( error, usuarioBorrado ) => {});

	Usuario.findByIdAndUpdate( id, cambiarEstado, updateOptions, ( error, usuarioDB ) => {

		if ( error ) {

			return response.status( 400 ).json({
				ok: false,
				error
			});
		}

		if ( !usuarioDB ) {

			return response.status( 400 ).json({
				ok: false,
				err: {
					message: 'Usuario no encontrado'
				}
			});
		}

		response.json({
			ok: true,
			usuario: usuarioDB
		});

	});
}); 

module.exports = app;