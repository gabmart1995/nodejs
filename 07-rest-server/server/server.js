require( './config/config' );

const express = require( 'express' );
const mongoose = require( 'mongoose' );

const app = express();

const bodyParser = require( 'body-parser' );

// parsea los datos a formato / x-www-form-urlencoded
app.use( bodyParser.urlencoded( { extended: false } ));

app.use( bodyParser.json() );

// ===============================================
// 	Rutas de la aplicación
// ===============================================
app.use( require( './routes/index' ) );

// ===============================================
// 	Conexion a la base de datos
// ===============================================

// opciones de conexion
let connectOptions = {
	useNewUrlParser: true,
	useCreateIndex: true
}; 

mongoose.connect( 'mongodb://localhost:27017/cafe', connectOptions, ( error, response ) => {

	if ( error ) throw error;

	console.log( 'Base de datos ONLINE' );
});


// ================================================
// 	Servidor
// ================================================
app.listen( process.env.PORT, () => {
	console.log( 'Escuchando en el puerto: ', process.env.PORT );
});