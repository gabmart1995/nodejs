console.log('Inicio del programa');

setTimeout( () => {
	console.log('primer timeout');
}, 3000 );

setTimeout( () => {
	console.log('segundo timeout');
}, 0 );

setTimeout( () => {
	console.log('tercer timeout');
}, 0 );

console.log('fin del programa');

/*
	Ciclo de vida de un proceso de nodejs (Proceso de ejecucion)

	app.js: nodejs tiene una funcion principal llamada main dentro de la pila de 
	procesos (Call stack) que ejecuta la primera funcion toma las variables 
	las asigna  y luego las elimina despues

	Para el app2 ejecuta la funcion main y verifica si existe otra funcion
	en este caso saludar la toma y ejecuta la linea codigo, cuando la funcion
	termina la saca de la linea de procesos. Ejecutando el resto del codigo
	si el main termina la elimina de la cola

	Para el app3 crea el main ejecuta la primera linea el primer timeout es
	una tarea asincrona node la registra y pasa a una seccion llamada node apis
	donde se ejecuta tareas asincronas. Pasa al segundo timeout al registrarlo
	tambien pasa al node api. Cuando se haya ejecutado el primer timeout se 
	ejecuta pasará a la cola de callbacks. Termina con la instruccion de console.log
	terminando el main y ejecuta las tareas asincronas y las coloca en la cola de 
	procesos lo ejecuta y los elimina.

*/